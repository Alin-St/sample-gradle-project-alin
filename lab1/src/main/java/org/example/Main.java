package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        runCalculator();
    }

    public static void runCalculator() {

        System.out.println("Running calculator...");

        while (true) {
            System.out.println("Please choose an option:");
            System.out.println("1. Add");
            System.out.println("2. Subtract");
            System.out.println("3. Multiply");
            System.out.println("4. Divide");
            System.out.println("5. Cancel");

            var scanner = new Scanner(System.in);
            int number = scanner.nextInt();

            if (number < 1 || number >= 5)
                return;

            System.out.println("First number: ");
            int firstNumber = scanner.nextInt();

            System.out.println("Second number: ");
            int secondNumber = scanner.nextInt();

            int result = 0;

            if (number == 1) {
                result = Calculator.add(firstNumber, secondNumber);
            } else if (number == 2) {
                result = Calculator.subtract(firstNumber, secondNumber);
            } else if (number == 3) {
                result = Calculator.multiply(firstNumber, secondNumber);
            } else {
                result = Calculator.divide(firstNumber, secondNumber);
            }

            System.out.println("The result is " + result);
        }
    }
}
