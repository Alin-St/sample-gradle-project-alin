import org.example.Calculator;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    @Test
    public void testAdd() {
        MatcherAssert.assertThat(null, Calculator.add(0, 0) == 0);
        MatcherAssert.assertThat(null, Calculator.add(1, 1) == 2);
        MatcherAssert.assertThat(null, Calculator.add(1232, 5432) == 6664);
    }

    @Test
    public void testSubtract() {
        MatcherAssert.assertThat(null, Calculator.subtract(0, 0) == 0);
        MatcherAssert.assertThat(null, Calculator.subtract(1, 1) == 0);
        MatcherAssert.assertThat(null, Calculator.subtract(1232, 5432) == -4200);
    }

    @Test
    public void testMultiply() {
        MatcherAssert.assertThat(null, Calculator.multiply(0, 0) == 0);
        MatcherAssert.assertThat(null, Calculator.multiply(1, 1) == 1);
        MatcherAssert.assertThat(null, Calculator.multiply(1232, 5432) == 6692224);
    }

    @Test
    public void testDivide() {
        MatcherAssert.assertThat(null, Calculator.divide(0, 1) == 0);
        MatcherAssert.assertThat(null, Calculator.divide(1, 1) == 1);
        MatcherAssert.assertThat(null, Calculator.divide(5432, 1232) == 4);
    }
}
